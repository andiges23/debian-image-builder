FROM debian:9.4

RUN apt-get update && apt-get -y upgrade

RUN apt install -y --no-install-recommends \
	xz-utils \
	binfmt-support \
	qemu \
	qemu-user-static \
	debootstrap \
	kpartx \
	lvm2 \
	dosfstools \
	ca-certificates	\
	rsync \
	git-core
